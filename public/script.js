let request = new XMLHttpRequest();
request.open("GET","./src/menu.json", false);
request.send(null);
var jsonData = JSON.parse(request.responseText);

let count = jsonData.data.length;
let order = [];
let s = 0;

sum ();
for (let i=0; i<count; i++) {
    let foodDiv = document.createElement('div');
    foodDiv.className = 'content-menu__items__block__item';
    foodDiv.id = 'food__item'+[i+1];
    document.getElementById("content-menu__items__block__item").append(foodDiv);

    let foodDivImg = document.createElement('div');
    let foodDivTitle = document.createElement('div');
    let foodDivPrice = document.createElement('div');
    let foodDivAvailable = document.createElement('div');

    foodDivImg.className = 'item__img';
    foodDivTitle.className = 'item__title';
    foodDivPrice.className = 'item__price';
    foodDivAvailable.className = 'item__price';

    foodDivImg.innerHTML = `<img src="${jsonData.data[i].img}" alt="">`;
    foodDivTitle.innerHTML = `<p class="item__title__text">${jsonData.data[i].title}</p>`
    foodDivPrice.innerHTML = `<p class="item__price__text">$${jsonData.data[i].price}</p>`
    foodDivAvailable.innerHTML = `<p class="item__av__text">${jsonData.data[i].available} boals available</p>`

    document.getElementById("food__item"+[i+1]).append(foodDivImg);
    document.getElementById("food__item"+[i+1]).append(foodDivTitle);
    document.getElementById("food__item"+[i+1]).append(foodDivPrice);
    document.getElementById("food__item"+[i+1]).append(foodDivAvailable);

}

let items = document.getElementsByClassName("content-menu__items__block__item");


for (let i=0; i<count; i++) {
    items[i].addEventListener('click', function () {
        if (order.includes(jsonData.data[i]) === false) {
                order.push(jsonData.data[i])} else {
                    id = order.indexOf(jsonData.data[i]);
                    q_current=order[id].q;
                    order[id].q = parseInt(q_current)+1;
                    delete_order ();
                }
                addToOrder ();
                sum ();
               
    })}

function addToOrder () {
    document.getElementById("order-content__order").innerHTML="";

    for ( let i=0; i<order.length; i++){
        let x = (order[i].id) -1;
                let order__item = document.createElement('div');
                let order1 = document.createElement('div');
                let order__item__box = document.createElement('div');
                let order__image = document.createElement('div');
                let order__title = document.createElement('div');
                let order__q = document.createElement('div');
                let order__price = document.createElement('div');
                let order2 = document.createElement('div');
                let order__comment = document.createElement('div');
                let order__delete = document.createElement('div');
                
                order__item.className = 'order-content__order__item'; order__item.id ='order__item'+[x+1];
                order1.className = 'order-content__order__item__1st'; order1.id ='item__1st'+[x+1];
                order__item__box.className = 'order__item'; order__item__box.id = 'order__box'+[x+1];
                order__image.className = 'order__image';
                order__title.className = 'order__title';
                order__q.className = 'order__q';
                order__price.className = 'order__price'; order__price.id = 'price'+[x+1];
                order2.className = 'order-content__order__item__2nd'; order2.id = 'item__2nd'+[x+1];
                order__comment.className = 'order__comment';
                order__delete.className = 'order__delete'; order__delete.id = 'order__delete';

                order__image.innerHTML = `<img class="order__image__item" src="${jsonData.data[x].img}" alt="">`;
                order__title.innerHTML = `<p class="medium">${jsonData.data[x].title}</p><p class="medium">${jsonData.data[x].price}</p>`;
                order__q.innerHTML = `<input class="order_q_item" type="number" name="order_q" id="order_q${jsonData.data[x].id}" value="${jsonData.data[x].q}" placeholder="1"></input>`
                order__price.innerHTML = `<p class="order__price__item medium" id=price_item${x}>$${(jsonData.data[x].price*jsonData.data[x].q).toFixed(2)}</p>`
                order[i].new_price = jsonData.data[x].price*jsonData.data[x].q;
                order__comment.innerHTML = `<input class="order__comment__item" type="text" name="order__comment" placeholder="You can write some comments">`
                order__delete.innerHTML = `<a href="#" onclick="delete_order();"><img id="button${jsonData.data[x].id}" src="img/main/Delete.svg" alt=""></a>`

                document.getElementById("order-content__order").append(order__item);
                document.getElementById('order__item'+[x+1]).append(order1);
                document.getElementById('item__1st'+[x+1]).append(order__item__box)
                document.getElementById('order__box'+[x+1]).append(order__image);
                document.getElementById('order__box'+[x+1]).append(order__title);
                document.getElementById('item__1st'+[x+1]).append(order__q);
                document.getElementById('item__1st'+[x+1]).append(order__price);
                document.getElementById('order__item'+[x+1]).append(order2);
                document.getElementById('item__2nd'+[x+1]).append(order__comment);
                document.getElementById('item__2nd'+[x+1]).append(order__delete);
};
}

function sum() {
    document.getElementById("order-content__order__sum__item").innerHTML="";
    s = 0;
    for (i=0; i<order.length; i++){
        s = s + order[i].new_price;
    };
    let order_sum = document.createElement("p")
    order_sum.innerHTML = "$ "+s.toFixed(2);
    order_sum.classList = "order__sum content-filter__li__text";
    document.getElementById('order-content__order__sum__item').append(order_sum);}


function delete_order () {
    let delete_action = document.getElementsByClassName("order__delete");
    for (let i=0; i<delete_action.length; i++) {
        delete_action[i].addEventListener('click', function () {
            order.splice(i,1)
            console.log(i);
            addToOrder()
            sum()
        })
    }
}