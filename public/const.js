let foodImg = ['img/main/1.png', 'img/main/2.png','img/main/3.png', 'img/main/1.png', 'img/main/2.png', 'img/main/3.png','img/main/1.png', 'img/main/2.png','img/main/3.png', 'img/main/1.png', 'img/main/2.png', 'img/main/3.png'];

let foodTitle = ['Spicy seasoned seafood noodles', 'Salted Pasta with mushroom sauce','Beef dumpling in hot and sour soup', 'Spicy seasoned seafood noodles', 'Salted Pasta with mushroom sauce','Beef dumpling in hot and sour soup','Spicy seasoned seafood noodles', 'Salted Pasta with mushroom sauce','Beef dumpling in hot and sour soup', 'Spicy seasoned seafood noodles', 'Salted Pasta with mushroom sauce','Beef dumpling in hot and sour soup'];

let foodPrice = ['$2.29', '$2.69', '$2.99','$2.29', '$2.69', '$2.99','$2.29', '$2.69', '$2.99','$2.29', '$2.69', '$2.99'];

let foodAvailable = ['10 boals available', '16 boals available', '20 boals available','10 boals available', '16 boals available', '20 boals available','10 boals available', '16 boals available', '20 boals available','10 boals available', '16 boals available', '20 boals available'];

let foodClass = ['item__img','item__title', 'item__price', 'item__available'];

let foodCount = foodImg.length;
let foodClassCount = foodClass.length;

export {foodImg, foodTitle, foodPrice, foodAvailable, foodCount, foodClass, foodClassCount};